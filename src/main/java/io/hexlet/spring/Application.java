package io.hexlet.spring;

import io.hexlet.spring.beans.HelloHexlet;
import io.hexlet.spring.interfaces.MyInterface;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "io.hexlet.spring")
public class Application {

    private static ApplicationContext applicationContext;

    public static void main(String[] args) {
        applicationContext =
                new AnnotationConfigApplicationContext(Application.class);

        applicationContext.getBean(MyInterface.class).hello();
    }
}
