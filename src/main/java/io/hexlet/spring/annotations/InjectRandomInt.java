package io.hexlet.spring.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Важно, когда пишите свои аннотации поменять RetentionPolicy на Runtime. По умолчанию Class
 *
 *  Source - только в исходниках видна
 *  Class - аннотация в байткод попадет, но через рефлексию достатоть не сможете
 *  Runtime - можно в рантайме достать аннотацию
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface InjectRandomInt {
    int min();
    int max();
}
