package io.hexlet.spring.beans;

import io.hexlet.spring.annotations.InjectRandomInt;
import io.hexlet.spring.annotations.PostProxy;
import io.hexlet.spring.annotations.Profiling;
import io.hexlet.spring.interfaces.MyInterface;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

@Component
@Profiling
public class HelloHexlet implements MyInterface {

    @InjectRandomInt(min = 2, max = 7)
    private int repeat;

    public HelloHexlet() {
        System.out.println("Phase 1");
        System.out.println(repeat);
    }

    @PostConstruct
    public void init() {
        System.out.println("Phase 2");
        System.out.println(repeat);
    }

    @PostProxy
    public void hello() {
        for (int i = 0; i < repeat; i++) {
            System.out.println("Hello hexlet");
        }
    }
}
