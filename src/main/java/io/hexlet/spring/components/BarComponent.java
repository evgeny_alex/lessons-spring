package io.hexlet.spring.components;

import org.springframework.stereotype.Component;

@Component("barComponent")
public class BarComponent implements MyComponent {

    public String format() {
        return "bar";
    }
}