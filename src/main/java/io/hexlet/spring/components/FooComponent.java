package io.hexlet.spring.components;

import org.springframework.stereotype.Component;

@Component
public class FooComponent implements MyComponent, NewInterface {

    public String format() {
        return "foo";
    }
}
