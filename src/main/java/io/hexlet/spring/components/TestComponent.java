package io.hexlet.spring.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class TestComponent {

    @Autowired
    @Qualifier("fooComponent")
    private MyComponent myComponent;

    @Autowired
    private NewInterface newInterface;
}
