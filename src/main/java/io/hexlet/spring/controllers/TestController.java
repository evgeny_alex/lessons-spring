package io.hexlet.spring.controllers;

import io.hexlet.spring.components.BarComponent;
import io.hexlet.spring.repository.TestRepository;
import io.hexlet.spring.services.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    private BarComponent barComponent;

    @Autowired
    private TestService testService;

    @Autowired
    private TestRepository testRepository;

    @GetMapping("/welcome")
    public String welcome() {
        return "Welcome";
    }
}
