package io.hexlet.spring.services;

import io.hexlet.spring.components.TestComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestService {

    @Autowired
    private TestComponent testComponent;
}
